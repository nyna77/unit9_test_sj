﻿namespace NY.Unit9
{
    using System;
    using UnityEngine;

    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Player player;
        public ScoreManager ScoreManager { get; private set; }

        public Player Player => player;
        public IScoreNotifier Score => ScoreManager;

        private void Awake()
        {
            ScoreManager = new ScoreManager();
        }

        private void Start()
        {
            player.Health.OnDead += PlayerDied;
        }

        private void PlayerDied()
        {
            Debug.Log("player died");
        }

        private void OnDestroy()
        {
            player.Health.OnDead -= PlayerDied;
        }
    }
}