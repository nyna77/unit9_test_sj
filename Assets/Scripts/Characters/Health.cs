﻿namespace NY.Unit9
{
    using System;
    using UnityEngine;

    public class Health : IHealthNotifier
    {
        public int MaxHealth { get; private set; }
        public event Action OnDead;
        public event Action<int> OnHit;

        private int currentHealth;
        public int CurrentHealth
        {
            get => currentHealth;
            private set
            {
                if (currentHealth <= 0) return;
                int hitValue = currentHealth - value;
                currentHealth =  Mathf.Clamp(value, 0, MaxHealth);
                OnHit?.Invoke(hitValue);
                if (currentHealth <= 0)
                {
                    OnDead?.Invoke();
                }
            }
        }

        public void Init(int maxHealth)
        {
            MaxHealth = maxHealth;
            currentHealth = MaxHealth;
        }

        public void GetDamage(int damgeAmount)
        {
            CurrentHealth -= damgeAmount;
        }
    }
}