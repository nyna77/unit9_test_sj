﻿namespace NY.Unit9
{
    using System;

    public interface IHealthNotifier
    {
        event Action OnDead;
        event Action<int> OnHit;
        int MaxHealth { get; }
        int CurrentHealth { get; }
    }
}