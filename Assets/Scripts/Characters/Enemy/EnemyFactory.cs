﻿namespace NY.Unit9
{
    using UnityEngine;

    public class EnemyFactory : MonoBehaviour
    {
        [SerializeField] Enemy enemyPrefab;
        private EnemyPool enemyPool;

        private void Awake()
        {
            enemyPool = new EnemyPool(20, transform, enemyPrefab);
        }

        public Enemy CreateNewEnemy(Vector3 position, Quaternion rotation)
        {
            var enemy = enemyPool.GetInstance();
            enemy.transform.position = position;
            enemy.transform.rotation = rotation;
            return enemy;
        }
    }
}