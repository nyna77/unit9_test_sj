﻿namespace NY.Unit9
{
    using UnityEngine;

    public class EnemyHealthView : MonoBehaviour
    {
        [SerializeField] private MeshRenderer meshRenderer;
        [SerializeField] private Character enemy;
        private MaterialPropertyBlock matBlock;
        private Camera mainCamera;

        private void Awake()
        {
            matBlock = new MaterialPropertyBlock();
            mainCamera = Camera.main;
            meshRenderer.GetPropertyBlock(matBlock);
        }

        private void LateUpdate()
        {
            AlignToCamera();
            UpdateParams();
        }

        private void UpdateParams()
        {
            matBlock.SetFloat("_Fill", enemy.Health.CurrentHealth / (float)enemy.Health.MaxHealth);
            meshRenderer.SetPropertyBlock(matBlock);
        }

        private void AlignToCamera()
        {
            var camXform = mainCamera.transform;
            var forward = transform.position - camXform.position;
            forward.Normalize();
            var up = Vector3.Cross(forward, camXform.right);
            transform.rotation = Quaternion.LookRotation(forward, up);
        }
    }
}