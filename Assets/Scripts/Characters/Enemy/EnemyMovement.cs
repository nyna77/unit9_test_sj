﻿namespace NY.Unit9
{
    using UnityEngine;
    using UnityEngine.AI;

    public class EnemyMovement : MonoBehaviour
    {
        [SerializeField] private NavMeshAgent agent;

        public void Init(MovementSettings movementSettings, Vector3 targetPosition)
        {
            agent.speed = movementSettings.Velocity;
            agent.angularSpeed = movementSettings.AngularSpeed;
            agent.stoppingDistance = movementSettings.StopDistance;
            agent.destination = targetPosition;
        }
    }
}