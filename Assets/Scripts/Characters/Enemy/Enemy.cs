﻿namespace NY.Unit9
{
    using System;
    using UnityEngine;

    public class Enemy : Character
    {
        [SerializeField] private EnemyMovement movement;
        [SerializeField] private EnemyAttack attack;

        public EnemyMovement Movement => movement;
        public EnemyAttack Attack => attack;

        /// <summary>
        /// fired when enemy is killed
        /// </summary>
        public event Action<Enemy> OnDead;

        private void OnEnable()
        {
            Health.OnDead += Dead;
            attack.enabled = true;
        }

        private void OnDisable()
        {
            Health.OnDead -= Dead;
        }

        private void Dead()
        {
            attack.enabled = false;
            //TODO: do some kill animation
            OnDead?.Invoke(this);
            Invoke(nameof(DisableMe), 1f);
        }
        
        private void DisableMe()
        {
            gameObject.SetActive(false);
        }
    }
}