﻿namespace NY.Unit9
{
    using UnityEngine;

    public class EnemySpawner : MonoBehaviour
    {
        [SerializeField] private Transform[] spawnPoints;
        [SerializeField] private EnemyFactory factory;
        [SerializeField] private EnemySettings enemySettings;
        //TODO: consider better reference
        [SerializeField] private ShootSystem shootSystem;

        //TODO: multiply(divide) enemy settings by difficulty
        public Enemy SpawnNewEnemy(float difficulty, Transform target)
        {
            Transform spawnPosition = GetSpawnPosition();
            var enemy = factory.CreateNewEnemy(spawnPosition.position, spawnPosition.rotation);
            enemy.Init(enemySettings.MaxHealth);
            enemy.Movement.Init(enemySettings.MovementSettings, target.position);
            enemy.Attack.Init(target, enemySettings.AttackSettings, shootSystem);
            return enemy;
        }

        private Transform GetSpawnPosition()
        {
            return spawnPoints[Random.Range(0, spawnPoints.Length)];
        }
    }
}