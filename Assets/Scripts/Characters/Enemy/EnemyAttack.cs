﻿namespace NY.Unit9
{
    using UnityEngine;

    public class EnemyAttack : BaseAttack
    {
        private Transform target;
        private EnemyAttackSettings enemyAttackSettings;

        public void Init(Transform target, EnemyAttackSettings enemyAttackSettings, ShootSystem shootSystem)
        {
            this.target = target;
            this.enemyAttackSettings = enemyAttackSettings;
            Init(shootSystem, enemyAttackSettings);
        }

        protected override Vector3 FindTarget()
        {
            return target.position + Random.insideUnitSphere * enemyAttackSettings.AccuracyRadius;
        }
    }
}