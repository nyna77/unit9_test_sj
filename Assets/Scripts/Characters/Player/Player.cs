﻿using UnityEngine;

namespace NY.Unit9
{
    public class Player : Character
    {
        [SerializeField] private PlayerAttack playerAttack;
        [SerializeField] private ShootSystem shootSystem;
        [SerializeField] private PlayerSettings playerSettings;

        private void Awake()
        {
            Init(playerSettings.MaxHealth);
            playerAttack.Init(shootSystem, playerSettings.AttackSettings);
        }
    }
}