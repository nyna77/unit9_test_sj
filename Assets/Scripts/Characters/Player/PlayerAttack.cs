﻿namespace NY.Unit9
{
    using UnityEngine;

    public class PlayerAttack : BaseAttack
    {
        [SerializeField] private GvrBasePointer gunPoint;

        protected override Vector3 FindTarget()
        {
            Vector3 target = gunPoint.CurrentRaycastResult.worldPosition;

            if (Mathf.Approximately(Vector3.Distance(Vector3.zero, target), 0f))
            {
                target = gunPoint.transform.forward * gunPoint.MaxPointerDistance;
            }

            return target;
        }
    }
}