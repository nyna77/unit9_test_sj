﻿using UnityEngine;
using UnityEngine.UI;

namespace NY.Unit9
{
    public class PlayerHealthView : MonoBehaviour
    {
        [SerializeField] private Player player;
        [SerializeField] private Slider slider;
        [SerializeField] private float damageSpeed;

        private IHealthNotifier Health => player.Health;

        private void OnEnable()
        {
            slider.maxValue = Health.MaxHealth;
            slider.value = Health.CurrentHealth;
        }

        private void Update()
        {
            if (!Mathf.Approximately(Health.CurrentHealth, slider.value))
            {
                slider.value = Mathf.Lerp(slider.value, Health.CurrentHealth, damageSpeed);
            }
        }
    }
}