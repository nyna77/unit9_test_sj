﻿using UnityEngine;

namespace NY.Unit9
{
    public class PlayerHitEffect : MonoBehaviour
    {
        [SerializeField] private MeshRenderer meshRenderer;
        [SerializeField] private Player player;

        private Material Material => meshRenderer.material;

        [SerializeField] private float viewTime;
        [SerializeField] private float scrollingDelay;
        [SerializeField] private float scrollingSpeed;
        [SerializeField] private float showTime;

        private const string treshold = "_Treshold";
        private const string mainTex = "_MainTex";

        //prevent triggering on start
        private float hitTime = -10;

        private void OnEnable()
        {
            player.Health.OnHit += DoHitEffect;
        }

        private void LateUpdate()
        {
            if (Time.time > hitTime + viewTime) return;

            float tresholdValue = Material.GetFloat(treshold);
            if (Time.time < hitTime + showTime)
            {
                tresholdValue = Mathf.Lerp(1f, .4f, (Time.time - hitTime) / showTime);
            }
            else
            { 
                tresholdValue = Mathf.Lerp(.4f, 1f, (Time.time - hitTime) / viewTime);
            }
            Material.SetFloat(treshold, tresholdValue);

            if (Time.time > hitTime + scrollingDelay)
            {
                Vector2 offset = Material.GetTextureOffset(mainTex);
                offset.y += Time.deltaTime * scrollingSpeed;
                Material.SetTextureOffset(mainTex, offset);
            }
        }

        private void DoHitEffect(int damageAmount)
        {
            TiggerHitEffect();
        }

        [ContextMenu(nameof(TiggerHitEffect))]
        private void TiggerHitEffect()
        {
            if (hitTime + viewTime > Time.time) return;
            hitTime = Time.time;
        }

        private void OnDisable()
        {
            player.Health.OnHit -= DoHitEffect;
        }
    }
}