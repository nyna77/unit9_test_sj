﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace NY.Unit9
{
    public class PlayerGunPoint : GvrBasePointer
    {
        public override float MaxPointerDistance => 30f;

        public event Action<GameObject> OnClick;

        private GameObject lastGazingObject;

        public override void GetPointerRadius(out float enterRadius, out float exitRadius)
        {
            enterRadius = 1f;
            exitRadius = 1f;
        }

        public override void OnPointerClickDown()
        {
            Debug.Log("click click");
            OnClick?.Invoke(lastGazingObject);
        }

        public override void OnPointerClickUp() {  }

        public override void OnPointerEnter(RaycastResult raycastResult, bool isInteractive)
        {
            lastGazingObject = raycastResult.gameObject;
        }

        public override void OnPointerExit(GameObject previousObject)
        {
            lastGazingObject = null;
        }

        public override bool IsAvailable => true;

        public override void OnPointerHover(RaycastResult raycastResultResult, bool isInteractive) { }
    }
}