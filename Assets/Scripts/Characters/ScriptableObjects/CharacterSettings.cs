﻿namespace NY.Unit9
{
    using UnityEngine;

    public abstract class CharacterSettings : ScriptableObject
    {
        [SerializeField] private int maxHealth;

        public int MaxHealth => maxHealth;
    }
}