﻿namespace NY.Unit9
{
    using UnityEngine;

    [CreateAssetMenu(menuName = "Unit9/Enemy")]
    public class EnemySettings : CharacterSettings
    {
        [SerializeField] private MovementSettings movementSettings;
        [SerializeField] private EnemyAttackSettings attackSettings;

        public MovementSettings MovementSettings => movementSettings;
        public EnemyAttackSettings AttackSettings => attackSettings;
    }
}