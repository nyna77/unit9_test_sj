﻿namespace NY.Unit9
{
    using UnityEngine;

    [CreateAssetMenu(menuName = "Unit9/Player")]
    public class PlayerSettings : CharacterSettings
    {
        [SerializeField] private AttackSettings attackSettings;
        public AttackSettings AttackSettings => attackSettings;
    }
}