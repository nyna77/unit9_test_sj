﻿namespace NY.Unit9
{
    using System;
    using UnityEngine;

    [Serializable]
    public class AttackSettings
    {
        [SerializeField] private float shootInterval;
        [SerializeField] private float shootForce;
        [SerializeField] private int damage;

        public float ShootInterval => shootInterval;
        public float ShootForce => shootForce;
        public int Damage => damage;
    }

    [Serializable]
    public class EnemyAttackSettings : AttackSettings
    {
        [SerializeField] private float accuracyRadius;
        public float AccuracyRadius => accuracyRadius;
    }
}