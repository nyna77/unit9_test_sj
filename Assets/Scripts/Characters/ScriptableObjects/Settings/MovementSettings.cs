﻿namespace NY.Unit9
{
    using System;
    using UnityEngine;
    
    [Serializable]
    public class MovementSettings
    {
        [SerializeField] private float stopDistance;
        [SerializeField] private float velocity;
        [SerializeField] private float angularSpeed;

        public float StopDistance => stopDistance;
        public float Velocity => velocity;
        public float AngularSpeed => angularSpeed;
    }
}