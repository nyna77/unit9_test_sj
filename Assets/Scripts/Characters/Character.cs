﻿namespace NY.Unit9
{
    using UnityEngine;

    public abstract class Character : MonoBehaviour
    {
        private Health _myHealth = new Health();
        public IHealthNotifier Health => _myHealth;

        public void Init(int maxHealth)
        {
            _myHealth.Init(maxHealth);
        }

        public void GetDamage(int amount)
        {
            //Debug.Log($"{name} got {amount} damage");
            _myHealth.GetDamage(amount);
        }
    }
}