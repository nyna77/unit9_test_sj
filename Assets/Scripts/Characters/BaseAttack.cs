﻿namespace NY.Unit9
{
    using System.Collections;
    using UnityEngine;

    public abstract class BaseAttack : MonoBehaviour
    {
        [SerializeField] protected Transform muze;
        private ShootSystem shootSystem;
        private AttackSettings attackSettings;
        private Coroutine shootCorutine;

        public void Init(ShootSystem shootSystem, AttackSettings attackSettings)
        {
            this.shootSystem = shootSystem;
            this.attackSettings = attackSettings;
            StartShooting();
        }

        private void StartShooting()
        {
            shootCorutine = StartCoroutine(ShootCorutine(attackSettings.ShootInterval));
        }

        private void OnDisable()
        {
            StopAllCoroutines();
        }

        protected virtual void Shoot(float force)
        {
            shootSystem.DoShoot(muze.position, FindTarget(), force, attackSettings.Damage);
            StartShooting();
        }

        private IEnumerator ShootCorutine(float delay)
        {
            yield return new WaitForSeconds(delay);
            Shoot(attackSettings.ShootForce);
        }

        protected abstract Vector3 FindTarget();
    }
}