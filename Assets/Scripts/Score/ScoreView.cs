﻿using UnityEngine;
using UnityEngine.UI;

namespace NY.Unit9
{
    public class ScoreView : MonoBehaviour
    {
        [SerializeField] private GameManager gameManager;
        [SerializeField] private Text scoreText;

        private IScoreNotifier Score => gameManager.Score;

        private void Start()
        {
            Score.OnNewScore += SetNewScore;
            SetNewScore(Score.ScoreValue);
        }

        private void SetNewScore(int score)
        {
            scoreText.text = string.Format("Score: {0}", score.ToString());
        }

        private void OnDestroy()
        {
            Score.OnNewScore -= SetNewScore;
        }
    }
}