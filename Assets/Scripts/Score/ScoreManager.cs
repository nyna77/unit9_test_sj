﻿using System;

namespace NY.Unit9
{
    public class ScoreManager : IScoreNotifier
    {
        private int score;
        public int ScoreValue
        {
            get => score;
            private set
            {
                score = value;
                OnNewScore?.Invoke(ScoreValue);
            }
        }
        public event Action<int> OnNewScore;

        public void AddScore(int score)
        {
            ScoreValue += score;
        }

        public void SetScore(int newScoreValue)
        {
            ScoreValue = newScoreValue;
        }
    }
}