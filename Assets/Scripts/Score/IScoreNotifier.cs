﻿using System;

namespace NY.Unit9
{
    public interface IScoreNotifier
    {
        event Action<int> OnNewScore;
        int ScoreValue { get; }
    }
}