﻿namespace NY.Unit9
{
    using UnityEngine;

    public class ProjectilePool : Pool<Projectile>
    {
        public ProjectilePool(int poolSize, Transform poolParent, Projectile prefab) : base(poolSize, poolParent, prefab)
        {
        }
    }
}