﻿namespace NY.Unit9
{
    using UnityEngine;

    public class EnemyPool : Pool<Enemy>
    {
        public EnemyPool(int poolSize, Transform poolParent, Enemy prefab) : base(poolSize, poolParent, prefab)
        {
        }
    }
}