﻿namespace NY.Unit9
{
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    public class Pool<T> where T : MonoBehaviour
    {
        private List<T> poolObjects = new List<T>();
        private int poolSize;
        private int activeObjects;
        private Transform poolParent;
        private T prefab;

        public Pool(int poolSize, Transform poolParent, T prefab)
        {
            this.poolSize = poolSize;
            this.poolParent = poolParent;
            this.prefab = prefab;

            InitPool();
        }

        private void InitPool()
        {
            for (int i = 0; i < poolSize; i++)
            {
                CreateNewInstance();
            }
        }

        private T CreateNewInstance()
        {
            var instance = GameObject.Instantiate(prefab, poolParent);
            instance.gameObject.SetActive(false);
            poolObjects.Add(instance);
            return instance;
        }

        public T GetInstance()
        {
            var instance = poolObjects.FirstOrDefault(x => !x.gameObject.activeInHierarchy);
            if (instance == null)
            {
                instance = CreateNewInstance();
                Debug.LogWarning($"objects from pool instantiated in runtime! object type = {typeof(T)}");
            }
            instance.gameObject.SetActive(true);
            return instance;
        }
    }
}