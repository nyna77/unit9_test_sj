﻿namespace NY.Unit9
{
    using UnityEngine;

    public class EnemyManager : MonoBehaviour
    {
        [SerializeField] private EnemySpawner enemySpawner;
        [SerializeField] private GameManager gameManager;

        private Player Player => gameManager.Player;
        private ScoreManager ScoreManager => gameManager.ScoreManager;

        private void Start()
        {
            //TODO: better spawn system with progress curve
            InvokeRepeating(nameof(SpawnEnemy), 1f, 4f);
        }

        private void SpawnEnemy()
        {
            var enemy = enemySpawner.SpawnNewEnemy(1f, Player.transform);
            enemy.OnDead += EnemyDied;
        }
        
        private void EnemyDied(Enemy enemy)
        {
            ScoreManager.AddScore(enemy.Health.MaxHealth);
            enemy.OnDead -= EnemyDied;
        }
    }
}