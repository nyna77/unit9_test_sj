﻿namespace NY.Unit9
{
    using UnityEngine;

    public class ProjectileFactory : MonoBehaviour
    {
        [SerializeField] private Projectile projectilePrefab;
        private ProjectilePool projectilePool;

        private void Awake()
        {
            projectilePool = new ProjectilePool(30, transform, projectilePrefab);
        }

        public Projectile GetProjectile()
        {
            return projectilePool.GetInstance();
        }
    }
}