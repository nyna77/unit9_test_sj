﻿namespace NY.Unit9
{
    using UnityEngine;

    public class ShootSystem : MonoBehaviour
    {
        [SerializeField] private ProjectileFactory projectileFactory;

        public void DoShoot(Vector3 startPos, Vector3 endPos, float velocity, int damage)
        {
            var projectile = projectileFactory.GetProjectile();
            projectile.transform.position = startPos;
            Vector3 force = CalculateForce(startPos, endPos, velocity);
            projectile.ShootMe(force, damage);
        }
        
        //source: http://physics.stackexchange.com/questions/56265/how-to-get-the-angle-needed-for-a-projectile-to-pass-through-a-given-point-for-t        
        private Vector3 CalculateForce(Vector3 startPos, Vector3 endPos, float velocity)
        {
            float g = Physics.gravity.magnitude;

            Vector3 distance = startPos - endPos;
            distance.y = 0f;
            float dX = distance.magnitude;
            float dY = -(startPos - endPos).y;

            float VSquare = velocity * velocity;

            float temp = (VSquare * (VSquare - 2 * g * dY)) / (g * g * dX * dX) - 1;

            if (temp < 0)
                temp = 0;

            float tan = VSquare / (g * dX);
            tan += -Mathf.Sqrt(temp);

            float angleRad = Mathf.Atan(tan);
            float angleDeg = angleRad * Mathf.Rad2Deg;

            Vector3 direcrion = endPos - startPos;
            direcrion.y = 0f;
            Vector3 initialVelocityVector = direcrion.normalized * velocity;
            initialVelocityVector = Quaternion.AngleAxis(angleDeg, Vector3.Cross(initialVelocityVector, Vector3.up).normalized) * initialVelocityVector;

            return initialVelocityVector;
        }
    }
}