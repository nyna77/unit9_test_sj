﻿namespace NY.Unit9
{
    using UnityEngine;

    public class Projectile : MonoBehaviour
    {
        [SerializeField] private new Rigidbody rigidbody;
        [SerializeField] private GameObject mainView;
        [SerializeField] private GameObject destroyedView;

        private int damage;

        private bool used;
        private bool Used
        {
            get => used;
            set
            {
                used = value;
                if (used)
                {
                    mainView.SetActive(false);
                    destroyedView.SetActive(true);
                    rigidbody.constraints = RigidbodyConstraints.FreezeAll;
                    Invoke(nameof(DisableMe), 2f);
                }
                else
                {
                    mainView.SetActive(true);
                    destroyedView.SetActive(false);
                    rigidbody.constraints = RigidbodyConstraints.None;
                }
            }
        }

        private void OnEnable()
        {
            Used = false;
        }

        public void ShootMe(Vector3 force, int damage)
        {
            this.damage = damage;
            rigidbody.AddForce(force, ForceMode.VelocityChange);
            rigidbody.AddTorque(force / 2f);
        }

        private void OnCollisionEnter(Collision collision)
        {
            var character = collision.gameObject.GetComponentInParent<Character>();
            if (character != null)
            {
                character.GetDamage(damage);
            }
            Used = true;
        }

        private void DisableMe()
        {
            gameObject.SetActive(false);
        }
    }
}